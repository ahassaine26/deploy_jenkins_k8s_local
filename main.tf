resource "kubernetes_namespace" "maven_ns" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_deployment" "deploy_maven" {
  metadata {
    name      = "deploy-maven"
    namespace = kubernetes_namespace.maven_ns.metadata[0].name
  }
  spec {
    replicas = var.replicas
    selector {
      match_labels = {
        app = "app_maven"
      }
    }
    template {
      metadata {
        labels = {
          app = "app_maven"
        }
      }
      spec {
        container {
          image = var.image
          name  = "mvn"
          port {
            container_port = 2222
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "maven_srv" {
  metadata {
    name      = "maven-srv"
    namespace = kubernetes_namespace.maven_ns.metadata[0].name
  }
  spec {
    selector = {
      app = kubernetes_deployment.deploy_maven.spec[0].template[0].metadata[0].labels.app
    }
    port {
      port        = 2222
      target_port = 2222
    }
    type = "NodePort"
  }
}

 

output "service_node_port" {
  value = kubernetes_service.maven_srv.spec[0].port[0].node_port
}
