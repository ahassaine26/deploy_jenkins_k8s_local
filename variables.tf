variable "namespace" {
  description = "namespace maven"
  default     = "maven-ns"
}

variable "image" {
  description = "The container image to deploy"
  default     = "anishassaine/pipline_docker:latest"
}

variable "replicas" {
  description = "replicas number"
  default     = 1
}